# Racket "wat"s

*inspired by Gary Bernhardt's [original "Wat" talk](https://www.destroyallsoftware.com/talks/wat)*

**This repository is an ongoing work in progress, and is intended to be a community
effort. If you have anything new wat's to contribute, or think one should be removed,
please create a pull request!**

# List of wat's

- Numbers are mutable

```scheme
Welcome to Racket v7.5.0.900.
> (immutable? 5)
#f
```

- Unquote can be weird:

```scheme
Welcome to Racket v7.5.0.900.
> `(4 . ,(+ 1 2))
(4 . 3)
> '(4 . ,(+ 1 2))
'(4 unquote (+ 1 2))
```

- Argument order for collections:

```scheme
Welcome to Racket v7.5.0.900.
> (member 'x (list 'x 'y))
#t
> (vector-member 'x (vector 'x 'y))
#t
> (set-member? (set 'x 'y) 'x)
#t
```

- `gensym` is special

```scheme
Welcome to Racket v7.5.0.900.
> (define x (gensym))
> x
'g78
> (eq? x 'g78)
#f
```

- `hash-map` doesn't return a hash

```scheme
Welcome to Racket v7.5.0.900.
> (map list '(1 2 3))
'((1) (2) (3))
> (vector-map list #(1 2 3))
'#((1) (2) (3))
> (hash-map (hash 'x 1 'y 2) list)
'((x 1) (y 2))  ; that's not a hash map...
```

- `curry`

```racket
Welcome to Racket v7.5.0.900.
> (((((((((((((((((((((((((((((((((curry add1)))))))))))))))))))))))))))))))))
#<procedure:curried:add1>
> ((((((((((((((((((((((((((((((((((curry add1))))))))))))))))))))))))))))))))) 5)
6
```

- **TODO** more
